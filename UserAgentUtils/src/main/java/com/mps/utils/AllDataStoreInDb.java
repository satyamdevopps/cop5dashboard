package com.mps.utils;
import java.sql.Connection;
import java.util.Map;
import com.mps.dbConnection.DbConnetion;
import com.mps.model.IpAddress;
import com.mps.model.IpCountry;
import com.mps.model.MyDataTable;
import com.mps.model.MyLogger;
import com.mps.model.Un_identified_fulltext;
import com.mps.model.UserTechnology;
public class AllDataStoreInDb {
	 static MyLogger myLogger = new MyLogger();
	  static  String[] monthname={"","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"};
	public  int fillUserAgent( Map<String, UserTechnology> allUserAgent, int  month) throws Exception{
		    Connection connection=new DbConnetion().mysqlConnection();
		    MyDataTable  mdt=new MyDataTable("dashboard_user_technology_analysis_v2");
			String  columnMonth=monthname[month];
		    mdt.addColumn("webmart_id", "0");
			 mdt.addColumn("device", "");
			 mdt.addColumn("os", "");
			 mdt.addColumn("os_version", "");
			 mdt.addColumn("browser", "");
			 mdt.addColumn("browser_version", "");
			 mdt.addColumn("year", "0");
			 mdt.addColumn("month", "0");
			 mdt.addColumn("set_no", "0");
			 mdt.addColumn(columnMonth, "0");
			 /*
		    String dynamicCol="M_"+"2019"+(month>9 ? month : "0"+month);
			 mdt.addColumn("device", "");
			 mdt.addColumn("os", "");
			 mdt.addColumn("os_version", "");
			 mdt.addColumn("browser", "");
			 mdt.addColumn("browser_version", "");
			 mdt.addColumn(dynamicCol, "0");
			 */
			 for(UserTechnology ut : allUserAgent.values()){
				  mdt.addRow();
				  mdt.updateData(mdt.getRowCount(),  "webmart_Id",ut.getWebmart_Id());
				  mdt.updateData(mdt.getRowCount(), "device", ut.getDevice_name());
			      mdt.updateData(mdt.getRowCount(), "os", ut.getOs());
			      mdt.updateData(mdt.getRowCount(), "os_version", ut.getOs_name());
			      mdt.updateData(mdt.getRowCount(), "browser", ut.getBrowser());
			      mdt.updateData(mdt.getRowCount(), "browser_version", ut.getBrowser_version());
			      mdt.updateData(mdt.getRowCount(), "year", String.valueOf(ut.getYear()));
			      mdt.updateData(mdt.getRowCount(), "month", String.valueOf(ut.getMonth()));
			      mdt.updateData(mdt.getRowCount(), "set_no", String.valueOf(ut.getSet_no()));
			      mdt.updateData(mdt.getRowCount(), columnMonth, String.valueOf(ut.getProcess_month()));
			}	 
			 
			int records = mdt.commit(connection);
			myLogger.log("UserAgent database storing...... "+records);
		return records;
	}
	
	
	public int fill_Ip_Address( Map<String, IpAddress> ipAddressInfo, int month ) throws Exception{
	    Connection connection=new DbConnetion().mysqlConnection();
	    /*MyDataTable  mdt=new MyDataTable("`c5_daskboard_highest_ip`");
		 mdt.addColumn("webmart_id", "0");
		 mdt.addColumn("ip_address", "");
		 mdt.addColumn("institution_id", "");
		 mdt.addColumn("institution_name", "");
		 mdt.addColumn("status", "");
		 mdt.addColumn("year", "0");
		 mdt.addColumn("month", "0");
		 mdt.addColumn("set_no", "0");
		 mdt.addColumn(columnMonth, "0");
		*/ 
	    
	    MyDataTable  mdt=new MyDataTable("c5_daskboard_highest_ip");
	    String dynamicCol="M_"+"2019"+(month>9 ? month : "0"+month);
		 mdt.addColumn("ip_address", "");
		 mdt.addColumn("institution_id", "");
		 mdt.addColumn("institution_name", "");
		 mdt.addColumn("status", "");
		 mdt.addColumn(dynamicCol, "0");
		 mdt.addColumn("publisher", "");
		 
	    
		 for(IpAddress ip : ipAddressInfo.values()){
			  mdt.addRow();
			  mdt.updateData(mdt.getRowCount(), "ip_address", ip.getIp_address());
		      mdt.updateData(mdt.getRowCount(), "institution_id", ip.getInstitution_id());
		      mdt.updateData(mdt.getRowCount(), "institution_name", ip.getInstitution_name());
		      mdt.updateData(mdt.getRowCount(), "status", ip.getStatus());
		      //String dynamicMonth = "M_"+ip.getYear()+""+ip.getMonth();
		      mdt.updateData(mdt.getRowCount(), dynamicCol, String.valueOf(ip.getCount()));
               mdt.updateData(mdt.getRowCount(), "publisher", ip.getPublisher());
		      
		}	 
		 
		int records = mdt.commit(connection);
		myLogger.log("IpAddress database storing...... "+records);
	return records;
}
	
	

	public int un_identified_fulltextfillDatabase( Map<String, Un_identified_fulltext> un_identified_fulltext, int month ) throws Exception{
	    Connection connection=new DbConnetion().mysqlConnection();
	    MyDataTable  mdt=new MyDataTable("c5_daskboard_un_identified_fulltext");
	    String dynamicCol="M_"+"2019"+(month>9 ? month : "0"+month);
	    mdt.addColumn("webmart_id", "");
		 mdt.addColumn("ip_address", "");
		 mdt.addColumn("country", "");
		 mdt.addColumn(dynamicCol, "0");
		 mdt.addColumn("publisher", "");
		 
	    
		 for(Un_identified_fulltext ip : un_identified_fulltext.values()){
			  mdt.addRow();
			  mdt.updateData(mdt.getRowCount(), "webmart_id", ip.getWebmart_id());
			  mdt.updateData(mdt.getRowCount(), "ip_address", ip.getIp_address());
		      mdt.updateData(mdt.getRowCount(), "country", ip.getCountry());
		      //String dynamicMonth = "M_"+ip.getYear()+""+ip.getMonth();
		      mdt.updateData(mdt.getRowCount(), dynamicCol, String.valueOf(ip.getCount()));
               mdt.updateData(mdt.getRowCount(), "publisher", ip.getPublisher());
		      
		}	 
		 
		int records = mdt.commit(connection);
		myLogger.log("un_identified_fulltext database storing...... "+records);
	return records;
}
	//for ip country store in data base
	public  int IpcountryfillDatabase( Map<String, IpCountry> ipCountry, int month ) throws Exception{
	    Connection connection=new DbConnetion().mysqlConnection();
	    MyDataTable  mdt=new MyDataTable("c5_daskboard_country_high_ip");
	    String dynamicCol="M_"+"2019"+(month>9 ? month : "0"+month);
	    mdt.addColumn("webmart_id", "");
		 mdt.addColumn("ip_address", "");
		 mdt.addColumn("country", "");
		 mdt.addColumn(dynamicCol, "0");
		 mdt.addColumn("publisher", "");
		 
	    
		 for(IpCountry ip : ipCountry.values()){
			  mdt.addRow();
			  mdt.updateData(mdt.getRowCount(), "webmart_id", ip.getWebmart_id());
			  mdt.updateData(mdt.getRowCount(), "ip_address", ip.getIp_address());
		      mdt.updateData(mdt.getRowCount(), "country", ip.getCountry());
		      //String dynamicMonth = "M_"+ip.getYear()+""+ip.getMonth();
		      mdt.updateData(mdt.getRowCount(), dynamicCol, String.valueOf(ip.getCount()));
               mdt.updateData(mdt.getRowCount(), "publisher", ip.getPublisher());
		      
		}	 
		 
		int records = mdt.commit(connection);
		myLogger.log("un_identified_fulltext database storing...... "+records);
	return records;
}
	
	public MyDataTable daskboardcountrycreateMDT(int month) {

		MyDataTable mdt = new MyDataTable("c5_daskboard_country_high_ip");
		try {
			String dynamicCol = "M_" + "2019" + (month > 9 ? month : "0" + month);
			mdt.addColumn("webmart_id", "");
			mdt.addColumn("ip_address", "");
			mdt.addColumn("country", "");
			mdt.addColumn(dynamicCol, "0");
			mdt.addColumn("publisher", "");
		} catch (Exception e) {
		}
		return mdt;
	}
	public int daskboardcountrycommitMdt(MyDataTable mdt) {
		Connection connection=null;
		int total=0;
		try {
			if (connection == null) {
				connection = new DbConnetion().mysqlConnection();
			}
			 total=mdt.commit(connection);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		 return total;
	}
	
	
	
	
	
	
	
	
	
	
}
