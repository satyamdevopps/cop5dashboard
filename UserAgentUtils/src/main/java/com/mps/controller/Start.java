package com.mps.controller;

import java.util.*;
import com.mps.businesslogic.High_Ip_Address_logic;
import com.mps.businesslogic.Highest_country_ip_logic;
import com.mps.businesslogic.Un_Identified_Fulltext_Logic;
import com.mps.businesslogic.User_Technology_Logic;
import com.mps.model.Methods;
import com.mps.model.MyConfiguration;
import com.mps.model.MyLogger;

import java.io.File;

public class Start {
	static MyLogger myLogger = new MyLogger();
	String rootPath = "";
	String configPath = "";
	String GeoIp = "";
	Methods methods = new Methods();
	@SuppressWarnings("resource")
	public HashMap<String, String> initialize() throws Exception {
		Methods methods = new Methods();
		rootPath = methods.getClassPath(this);
		configPath = rootPath + File.separator + "config" + File.separator + "config.mps";
		GeoIp = rootPath + File.separator + "config" + File.separator + "GeoIP.dat";
		return methods.readPropertyFile(configPath);
	}

	public static void main(String[] args) throws Exception {
		Start start = new Start();
		HashMap<String, String> configMap = start.initialize();
		MyConfiguration configuration = new MyConfiguration();
		configuration.setPublisher(configMap.get("publisher.name"));
		configuration.setMonth(configMap.get("publisher.month"));
		configuration.setWebmart_id(configMap.get("publisher.webmart_id"));
		configuration.setYear(configMap.get("publisher.year"));
		configuration.setMongoDriver(configMap.get("mongo.db.driver"));
		configuration.setGeoIpPath(start.GeoIp);
		String user_agent = configMap.get("publisher.user_agent");
		String Ip_Address = configMap.get("publisher.high_Ip_Address");
		String un_identified_fulltext = configMap.get("publisher.un_identified_fulltext");
		String highestCountryIp = configMap.get("publisher.highestCountryIp");
		if (user_agent.equalsIgnoreCase("true")) {
			myLogger.log("done processing user_Agent");
			User_Technology_Logic utl = new User_Technology_Logic();
			int result = utl.runUserTechology(configuration);
			myLogger.log(result + "sucucessfuly fill  User Agent in database  ");
		}
		if (Ip_Address.equalsIgnoreCase("true")) {
			myLogger.log("processing for Ip Address");
			High_Ip_Address_logic hia = new High_Ip_Address_logic();
			int result = hia.runIpAddress(configuration);
			myLogger.log(result + "sucucessfuly fill  ip address in database");
		}
		if (un_identified_fulltext.equals("true")) {
			Un_Identified_Fulltext_Logic uifl = new Un_Identified_Fulltext_Logic();
			myLogger.log("Processing for un_identified_fulltext");
			int un_ip = uifl.rununidentifiedfulltext(configuration);
			myLogger.log(un_ip + "sucucessfuly fill  un_identified_fulltext  in dataBase");
		}
		if (highestCountryIp.equals("true")) {
			Highest_country_ip_logic hcil = new Highest_country_ip_logic();
			myLogger.log("Processing for Country high IP ");
			int total = hcil.runhighestipcountry(configuration);
			myLogger.log(total + "sucucessfuly fill  country high Ip  in dataBase");
		}
	}
}
