package com.mps.businesslogic;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mps.model.IpAddress;
import com.mps.model.MyConfiguration;
import com.mps.utils.AllDataStoreInDb;

public class High_Ip_Address_logic {
	
        Map<String, IpAddress> Ipfreq = new HashMap<String, IpAddress>();
        public int runIpAddress(MyConfiguration configuration) throws NumberFormatException, Exception
        {    
        	AllDataStoreInDb adsi=new AllDataStoreInDb();
        	findIp_AddressInfo(Integer.parseInt(configuration.getMonth()),
        			Integer.parseInt(configuration.getYear()), 
        			configuration.getPublisher(),
        			configuration.getWebmart_id(),
        			configuration.getMongoDriver());
        	int result=adsi.fill_Ip_Address(Ipfreq, Integer.parseInt(configuration.getMonth()));
        	return result;
        }
	public  void findIp_AddressInfo(int month, int year, String client,
			String webmart_id,String mongoDriver ) {
		String dbName = client + "_counter";
		MongoClient mongo = new MongoClient(mongoDriver, 27017);
		MongoDatabase mongoDb = mongo.getDatabase(dbName);
		String collName = "counter_" + year+ month;
		MongoCollection<Document> collection = mongoDb.getCollection(collName);
		AggregateIterable<Document> agr = collection
				.aggregate(Arrays.asList(Aggregates.match(Filters.eq("page_metric_id", 8)),
						Aggregates.group(new Document("ip_address", "$ip_address")
								.append("institution_codes", "$institution_codes").append("page_metric_id",
										"$page_metric_id"),
								Accumulators.sum("count", 1)),
						Aggregates.sort((new Document("count", -1))), Aggregates.limit(500)))
				.allowDiskUse(true);
		for (Document doc : agr) {

			Document docIn = (Document) doc.get("_id");
			String ip_address = docIn.get("ip_address").toString();
			String count = doc.get("count").toString();
			String institution_codes = docIn.get("institution_codes").toString();
			if (!"".equals(institution_codes)) {
				findIpAddressDetails(month,year,webmart_id,ip_address, count, institution_codes,client);
			}
		}
		mongo.close();
	}
	

	public  void findIpAddressDetails(int month,int year, String webmart_id,String ip_address, String count, String institution_codes,String publisher) {
		String status = "Active";
		IpAddress ip = new IpAddress();
		String ipkey = ip_address;
		ip.setWebmart_Id(webmart_id);
		ip.setIp_address(ip_address);
		ip.setInstitution_id(institution_codes);
		ip.setInstitution_name("satyam  kumar singh  ");
		ip.setYear(String.valueOf(year));
		ip.setMonth(String.valueOf(month));
		ip.setSetNo("7071");
		ip.setCount(count);
		ip.setStatus(status);
		ip.setPublisher(publisher);
		Ipfreq.put(ipkey, ip);

	}

}
