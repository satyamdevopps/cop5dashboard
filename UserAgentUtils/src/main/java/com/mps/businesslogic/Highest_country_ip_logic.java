package com.mps.businesslogic;

import java.util.Arrays;
import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mps.model.MyConfiguration;
import com.mps.model.MyDataTable;
import com.mps.model.MyLogger;
import com.mps.utils.AllDataStoreInDb;

public class Highest_country_ip_logic {
	MyLogger myLogger = new MyLogger();
	public int runhighestipcountry(MyConfiguration configuration) throws NumberFormatException, Exception {
		myLogger.log("HighestIpUsageInAllCountry running");
		 int result=HighestIpUsageInAllCountry(Integer.parseInt(configuration.getMonth()),
				Integer.parseInt(configuration.getYear()), configuration.getPublisher(), configuration.getWebmart_id(),
				configuration.getMongoDriver());
		return result;
	}
	public  int HighestIpUsageInAllCountry(int month, int year, String client, String webmart_id,
		String mongoDriver) throws Exception {
		String dbName = client + "_counter";
		Global_logic gl = new Global_logic();
		AllDataStoreInDb adsi=new AllDataStoreInDb();
		MongoClient mongo = new MongoClient(mongoDriver, 27017);
		MongoDatabase mongoDb = mongo.getDatabase(dbName);
		String collName = "counter_" + year + (month > 9 ? month : "0" + month);
		MongoCollection<Document> collection = mongoDb.getCollection(collName);
		AggregateIterable<Document> agr = collection.aggregate(Arrays.asList(
				Aggregates.match(Filters.eq("page_metric_id", 8)),
				Aggregates.group(new Document("ip_address", "$ip_address").append("page_metric_id", "$page_metric_id"),
						Accumulators.sum("count", 1)),
				Aggregates.sort((new Document("count", -1))))).allowDiskUse(true);
		   MyDataTable mdt = adsi.daskboardcountrycreateMDT(month);
	       String dynamicCol = "M_" +year + (month > 9 ? month : "0" + month);
	       int total=0;
		for (Document doc : agr) {
			Document docIn = (Document) doc.get("_id");
			String ip_address = docIn.get("ip_address").toString();
			String count = doc.get("count").toString();
			String country = gl.getCountry(ip_address);
			mdt.addRow();
			mdt.updateData(mdt.getRowCount(), "webmart_id", webmart_id);
			mdt.updateData(mdt.getRowCount(), "ip_address", ip_address);
			mdt.updateData(mdt.getRowCount(), "country", country);
			mdt.updateData(mdt.getRowCount(), dynamicCol, String.valueOf(count));
			mdt.updateData(mdt.getRowCount(), "publisher", client);
			if (mdt.getRowCount() >= 150000) {
				 total=adsi.daskboardcountrycommitMdt(mdt);
				mdt = null;
				mdt = adsi.daskboardcountrycreateMDT(month);
			}
		}
		 int lasttotal=adsi.daskboardcountrycommitMdt(mdt);
		 total=total+lasttotal;
		mdt = null;
		mongo.close();
		return total;
	}
}
