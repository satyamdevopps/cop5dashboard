package com.mps.businesslogic;

import java.io.File;
import java.io.IOException;

import com.maxmind.geoip.LookupService;
import com.mps.model.MyConfiguration;

public class Global_logic {

	    String getCountry(String ip) throws IOException {
	    MyConfiguration configuration=new MyConfiguration();
		File geoIpFile = new File(configuration.getGeoIpPath());
		LookupService cl = new LookupService(geoIpFile, LookupService.GEOIP_MEMORY_CACHE);
		String country = cl.getCountry(ip).getName();
		return country;
	}
}
