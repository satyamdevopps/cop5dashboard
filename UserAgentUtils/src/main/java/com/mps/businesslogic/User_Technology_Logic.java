package com.mps.businesslogic;

import java.time.YearMonth;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mps.model.MyConfiguration;
import com.mps.model.MyLogger;
import com.mps.model.UserTechnology;
import com.mps.utils.AllDataStoreInDb;

import eu.bitwalker.useragentutils.UserAgent;

public class User_Technology_Logic {

	Map<String, UserTechnology> userTechfreq = new HashMap<String, UserTechnology>();
	MyLogger myLogger = new MyLogger();

	public int runUserTechology(MyConfiguration configuration) throws NumberFormatException, Exception {
		AllDataStoreInDb adsi = new AllDataStoreInDb();
		myLogger.log("working processing userAgent:-");
		userTechAnalysis(Integer.parseInt(configuration.getMonth()), Integer.parseInt(configuration.getYear()),
				configuration.getPublisher(), configuration.getWebmart_id(), configuration.getMongoDriver());
		int result = adsi.fillUserAgent(userTechfreq, Integer.parseInt(configuration.getMonth()));
		return result;
	}
	public void userTechAnalysis(int month, int year, String client, String webmart_id, String mongoDriver) {
		String dbName = client + "_filter";
		MongoClient mongo = new MongoClient(mongoDriver, 27017);
		MongoDatabase mongoDb = mongo.getDatabase(dbName);
		YearMonth yearMonthObject = YearMonth.of(year, month);
		int daysInMonth = yearMonthObject.lengthOfMonth();
		for (int i = 1; i <= daysInMonth; i++) {
			myLogger.log("working processing userAgent:-"+i);
			String collName = "filter_" + year + "" + String.format("%02d", month) + "" + String.format("%02d", i);
			MongoCollection<Document> collection = mongoDb.getCollection(collName);
			AggregateIterable<Document> agr = collection
					.aggregate(Arrays.asList(Aggregates.group("$user_agent", Accumulators.sum("count", 1))))
					.allowDiskUse(true);
			for (Document doc : agr) {
				String count = doc.get("count").toString();
				String user_agent = doc.get("_id").toString();
				findUserInfo(month, year, user_agent, count, webmart_id);
			}
		}
		mongo.close();
	}

	public void findUserInfo(int month, int year, String userAgent, String count, String webmart_id) {

		String allowedOS = "Windows,Ubuntu,Symbian,SunOS,Others,MAC,Linux,iOS,Chrome,BlackBerry,Android";
		String allowedDevices = "Computer,Mobile,Others,Tablet";
		String allowedBrowser = "Chrome,Others,Downloading Tool,Firefox,Internet Explorer,Mozilla,Opera,Safari,SEAMONKEY,Silk,Thunderbird,Vivaldi,Edge,Outlook";
		UserTechnology userTech = null;
		String[] allowedOSArray = allowedOS.split(",");
		String[] allowedBrowserArray = allowedBrowser.split(",");
		String[] allowedDeviceArray = allowedDevices.split(",");
		int flag = 0;
		int s = 0;
		if (userAgent != null) {
			UserAgent ua = UserAgent.parseUserAgentString(userAgent);

			if (ua.getOperatingSystem().getDeviceType().getName() == null
					|| ua.getOperatingSystem().getDeviceType().getName().trim().equalsIgnoreCase("Unknown"))
				flag = 1;

			if (ua.getOperatingSystem().getName() == null
					|| ua.getOperatingSystem().getName().trim().equalsIgnoreCase("Unknown"))
				flag = 1;

			if (ua.getOperatingSystem().getGroup() == null
					|| ua.getOperatingSystem().getName().trim().equalsIgnoreCase("Unknown"))
				flag = 1;
			userTech = new UserTechnology();
			if (flag == 0) {

				String keyUserTech = "";
				userTech.setWebmart_Id(webmart_id);
				keyUserTech = webmart_id;
				for (int i = 0; i < allowedDeviceArray.length; i++) {
					if (ua.getOperatingSystem().getDeviceType().getName().toUpperCase()
							.contains(allowedDeviceArray[i].toUpperCase())) {
						userTech.setDevice_name(allowedDeviceArray[i]);
						keyUserTech = keyUserTech + allowedDeviceArray[i];
						s = 1;
						break;
					}

				}
				if (s == 0) {
					userTech.setDevice_name("Others");
					keyUserTech = keyUserTech + "Others";
				}

				s = 0;
				for (int i = 0; i < allowedOSArray.length; i++) {
					if (ua.getOperatingSystem().getName().toUpperCase().contains(allowedOSArray[i].toUpperCase())) {
						userTech.setOs(allowedOSArray[i]);
						keyUserTech = keyUserTech + allowedOSArray[i];
						s = 1;
						break;
					}
				}
				if (s == 0) {
					userTech.setOs("Others");
					keyUserTech = keyUserTech + "Others";
				}
				s = 0;
				userTech.setOs_name(ua.getOperatingSystem().getName().trim());
				keyUserTech = keyUserTech + ua.getOperatingSystem().getName().trim();
				for (int i = 0; i < allowedBrowserArray.length; i++) {
					if (ua.getBrowser().getName().trim().toString().toUpperCase()
							.contains(allowedBrowserArray[i].toUpperCase())) {
						if (ua.getBrowser().getName().trim().toString().toUpperCase().contains("IE")) {
							userTech.setBrowser("Internet Explorer");
							keyUserTech = keyUserTech + "Internet Explorer";
						} else {
							userTech.setBrowser(allowedBrowserArray[i]);
							keyUserTech = keyUserTech + allowedBrowserArray[i];
						}
						s = 1;
						break;
					}
				}
				if (s == 0) {
					userTech.setBrowser("Others");
					keyUserTech = keyUserTech + "Others";
				}
				s = 0;
				keyUserTech = keyUserTech + ua.getBrowser().getName().trim();
				userTech.setBrowser_version(ua.getBrowser().getName().trim());
				userTech.setYear(year);
				userTech.setMonth(month);
				userTech.setSet_no(4042);
				userTech.setProcess_month(Integer.parseInt(count));

				if (userTechfreq.containsKey(keyUserTech)) {
					userTech.setProcess_month(
							userTechfreq.get(keyUserTech).getProcess_month() + userTech.getProcess_month());
					userTechfreq.put(keyUserTech, userTech);
				} else {
					userTechfreq.put(keyUserTech, userTech);
				}
			}
			flag = 0;
		}
	}
}
