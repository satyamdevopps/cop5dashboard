package com.mps.businesslogic;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mps.model.MyConfiguration;
import com.mps.model.Un_identified_fulltext;
import com.mps.utils.AllDataStoreInDb;

public class Un_Identified_Fulltext_Logic {
	static Map<String, Un_identified_fulltext> un_identified_fulltextfreq = new HashMap<String, Un_identified_fulltext>();
	
	   public int rununidentifiedfulltext(MyConfiguration configuration) throws NumberFormatException, Exception
       {    
       	AllDataStoreInDb adsi=new AllDataStoreInDb();
       	un_identified_fulltext(Integer.parseInt(configuration.getMonth()),
       			Integer.parseInt(configuration.getYear()), 
       			configuration.getPublisher(),
       			configuration.getWebmart_id(),
       			configuration.getMongoDriver());
       	int result=adsi.un_identified_fulltextfillDatabase(un_identified_fulltextfreq, Integer.parseInt(configuration.getMonth()));
       	return result;
       }
	
	
	public void un_identified_fulltext(int month, int year, String client,
			String webmart_id,String mongoDriver) throws IOException {
		String dbName = client + "_counter";
		MongoClient mongo = new MongoClient(mongoDriver, 27017);
		MongoDatabase mongoDb = mongo.getDatabase(dbName);
		String collName = "counter_" + year+(month>9 ? month : "0"+month);
		MongoCollection<Document> collection = mongoDb.getCollection(collName);
		AggregateIterable<Document> agr = collection
				.aggregate(Arrays.asList(Aggregates.match(Filters.eq("page_metric_id", 8)),
						Aggregates.group(new Document("ip_address", "$ip_address")
								.append("institution_codes", "$institution_codes").append("page_metric_id",
										"$page_metric_id"),
								Accumulators.sum("count", 1)),
						Aggregates.sort((new Document("count", -1))), Aggregates.limit(100000)))
				.allowDiskUse(true);
		for (Document doc : agr) {
			Document docIn = (Document) doc.get("_id");
			String ip_address = docIn.get("ip_address").toString();
			String count = doc.get("count").toString();
			String institution_codes = docIn.get("institution_codes").toString();
			if (institution_codes.trim().equals("")||institution_codes.trim().equals("-")) {
				un_identified_fulltext(month,year,webmart_id,ip_address, count, institution_codes,client);
			}
		}
		
		mongo.close();
	}

	
   public  void un_identified_fulltext(int month,int year, String webmart_id,String ip_address, String count, String institution_codes,String publisher) throws IOException {
	
		Un_identified_fulltext uif=new Un_identified_fulltext();
		Global_logic gl=new Global_logic();
		String ipkey = ip_address;
		uif.setWebmart_id(webmart_id);
		uif.setIp_address(ip_address);
		uif.setCountry(gl.getCountry(ip_address));
		uif.setCount(count);
		uif.setPublisher(publisher);
		un_identified_fulltextfreq.put(ipkey, uif);
   }	
}
