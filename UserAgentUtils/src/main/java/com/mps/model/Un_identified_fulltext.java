package com.mps.model;

public class Un_identified_fulltext { 
	
	private String  webmart_id;
	private String ip_address;
	private String country;
	private String year;
	private String institution_id;
	private String status;
	private String count;
	private String publisher;
	
	public synchronized String getWebmart_id() {
		return webmart_id;
	}
	public synchronized void setWebmart_id(String webmart_id) {
		this.webmart_id = webmart_id;
	}
	public synchronized String getIp_address() {
		return ip_address;
	}
	public synchronized void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public synchronized String getCountry() {
		return country;
	}
	public synchronized void setCountry(String country) {
		this.country = country;
	}
	public synchronized String getYear() {
		return year;
	}
	public synchronized void setYear(String year) {
		this.year = year;
	}
	public synchronized String getInstitution_id() {
		return institution_id;
	}
	public synchronized void setInstitution_id(String institution_id) {
		this.institution_id = institution_id;
	}
	public synchronized String getStatus() {
		return status;
	}
	public synchronized void setStatus(String status) {
		this.status = status;
	}
	public synchronized String getCount() {
		return count;
	}
	public synchronized void setCount(String count) {
		this.count = count;
	}
	public synchronized String getPublisher() {
		return publisher;
	}
	public synchronized void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	
	
	
	
	
	

}
 