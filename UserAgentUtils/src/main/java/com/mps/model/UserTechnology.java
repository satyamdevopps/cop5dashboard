package com.mps.model;

public class UserTechnology {
	private String webmart_Id;
	private String device_name;
	private String os;
	private String os_name;
	private String browser;
	private String browser_version;
	private int year;
	private int month;
	private int process_month;
	private int set_no;
	public String getWebmart_Id() {
		return webmart_Id;
	}
	public void setWebmart_Id(String webmart_Id) {
		this.webmart_Id = webmart_Id;
	}
	public String getDevice_name() {
		return device_name;
	}
	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getOs_name() {
		return os_name;
	}
	public void setOs_name(String os_name) {
		this.os_name = os_name;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getBrowser_version() {
		return browser_version;
	}
	public void setBrowser_version(String browser_version) {
		this.browser_version = browser_version;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getProcess_month() {
		return process_month;
	}
	public void setProcess_month(int process_month) {
		this.process_month = process_month;
	}
	public int getSet_no() {
		return set_no;
	}
	public void setSet_no(int set_no) {
		this.set_no = set_no;
	}
	@Override
	public String toString() {
		return "UserTechnology [webmart_Id=" + webmart_Id + ", device_name="
				+ device_name + ", os=" + os + ", os_name=" + os_name
				+ ", browser=" + browser + ", browser_version="
				+ browser_version + ", year=" + year + ", month=" + month
				+ ", process_month=" + process_month + ", set_no=" + set_no
				+ "]";
	}
	
	
	
}
