package com.mps.model;

public class IpAddress {
	private String webmart_Id;
	private String ip_address;
	private String institution_id;
	private String year;
	private String month;
	private String institution_name;
	private String status;
	private String setNo;
	private String count;
	private String publisher;
	
	public String getWebmart_Id() {
		return webmart_Id;
	}
	public void setWebmart_Id(String webmart_Id) {
		this.webmart_Id = webmart_Id;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public String getInstitution_id() {
		return institution_id;
	}
	public void setInstitution_id(String institution_id) {
		this.institution_id = institution_id;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getInstitution_name() {
		return institution_name;
	}
	public void setInstitution_name(String institution_name) {
		this.institution_name = institution_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSetNo() {
		return setNo;
	}
	public void setSetNo(String setNo) {
		this.setNo = setNo;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public synchronized String getPublisher() {
		return publisher;
	}
	public synchronized void setPublisher(String publisher) {
		this.publisher = publisher;
	}

}
