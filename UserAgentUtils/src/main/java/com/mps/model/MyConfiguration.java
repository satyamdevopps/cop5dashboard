package com.mps.model;

public class MyConfiguration {
	 private String publisher;
	 private String month;
	 private String year;
	 private String mongoDriver;
	 private String webmart_id;
	 private String GeoIpPath;
	public synchronized String getPublisher() {
		return publisher;
	}
	public synchronized void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public synchronized String getMonth() {
		return month;
	}
	public synchronized void setMonth(String month) {
		this.month = month;
	}
	public synchronized String getYear() {
		return year;
	}
	public synchronized void setYear(String year) {
		this.year = year;
	}
	public synchronized String getMongoDriver() {
		return mongoDriver;
	}
	public synchronized void setMongoDriver(String mongoDriver) {
		this.mongoDriver = mongoDriver;
	}
	public synchronized String getWebmart_id() {
		return webmart_id;
	}
	public synchronized void setWebmart_id(String webmart_id) {
		this.webmart_id = webmart_id;
	}
	public synchronized String getGeoIpPath() {
		return GeoIpPath;
	}
	public synchronized void setGeoIpPath(String geoIpPath) {
		GeoIpPath = geoIpPath;
	}
	

}
