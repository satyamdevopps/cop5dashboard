package com.mps.dbConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;

import com.mps.controller.Start;

public class DbConnetion {
	
	
	
	public  Connection mysqlConnection() throws Exception
	{
		 Start start=new Start();
		 HashMap<String, String> configMap= start.initialize();	
		 String url=configMap.get("publisher.db.url");
		 String username=configMap.get("publisher.db.user");
		 String password=configMap.get("publisher.db.password");
		
		Class.forName("com.mysql.jdbc.Driver");  
		Connection con=DriverManager.getConnection(  
				url,username,password);
		return con;
	}

}
